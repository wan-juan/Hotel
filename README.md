# 酒店管理系统（多酒店）

## APP+H5+小程序预订

## 安装手册（必看）
> 安装手册：
    
    (1)sql在根目录(hotel.sql)，
        需要把表导入自己新建的数据库（默认数据库名hotel，需要自己去项目配置更改）
	
    【注意：必须配置隐藏index.php】
    (2)在Nginx低版本中，是不支持PATHINFO的，但是可以通过在Nginx.conf中配置转发规则实现：
        location / { // …..省略部分代码
           if (!-e $request_filename) {
                rewrite  ^(.*)$  /index.php?s=/$1  last;
            }
        }
    (3)PHP版本推荐7.0+   
	(4) /vendor/GatewayWorker/start_for_win.bat(需要自己执行)
    【注意：这个必须开启（需要配置PHP的环境变量才能执行）】

<html>
<!--在这里插入内容-->
<div></div>
</html>

> 项目配置

    1.需要到vendor目录下启动== Workerman==
    [link](https://www.workerman.net/)
    2.需要自行配置个推
    [link](https://dev.getui.com/)

>  项目架构
    

| 后台      |    PC酒店管理前台| App+H5+小程序|	内部APP|
| :--------: | :--------:| :--: |:--:|
| 创建分店|入住办理|酒店预订|内部员工使用|
| APP管理|管理房间|店内服务| |
| 接口管理|语音提示| |
|城市管理|交班丶夜审丶物品租借| |
|软硬件管理|更多...| |





## 前台地址 
    http://域名/home/login/index
    前台账号： wode/12345  
    	  dalong/123456
    	  .......
 
## 后台地址 
    http://域名/index/login/index
    后台台账号： bool/12345  



## qq群：484043598

<div  align="center">    
  <img src="./demo/qq.png" width = "400" alt="图片名称" align=center />
</div>


## 演示


![1](./demo/login.gif)

![1](./demo/rooms.gif)

![1](./demo/moves.gif)

![1](./demo/admin.gif)


## APP


![1](./demo/1.gif)

![1](./demo/2.gif)

![1](./demo/3.gif)




#-----------------------------------------------------------------------------------------------------

# 酒店系统新版本开发中

ThinkPHP 6.0版酒店管理系统
==============================
新版本对系统架构做了进一步的改进，其主要特性包括：

 + 入住管理
 + 预订管理
 + 语音提示、语音播报
 + 房态实时图表数据
 + 预警提示
 + 微信api接口
 + 短信营销
 + 会员管理
 + 酒店的商品管理
 + 报表中心
 + 财务管理
 + 设备管理。智能门锁、读卡器、身份证识别
 
> 系统的运行环境要求PHP7.2以上。
