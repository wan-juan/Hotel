<?php /*a:4:{s:51:"D:\phpstudy_pro\WWW\tp\view\home\welcome\index.html";i:1604891698;s:51:"D:\phpstudy_pro\WWW\tp\view\home\common\static.html";i:1603931011;s:54:"D:\phpstudy_pro\WWW\tp\view\home\common\resources.html";i:1603609812;s:56:"D:\phpstudy_pro\WWW\tp\view\home\common\room_select.html";i:1604889652;}*/ ?>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
    <meta charset="UTF-8">
    <title><?php echo htmlentities($system['hotel_name']); ?>(多酒店版)</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="stylesheet" href="/static/admin/css/font.css">
    <link rel="stylesheet" href="/static/admin/css/xadmin.css">
    <script src="/static/admin/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="/static/admin/js/xadmin.js"></script>

    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/2.0.3/jquery.js"></script>
    <script src="/static/jquery.printarea.js"></script>

    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


    <link href="/static/toastr/toastr.css" rel="stylesheet"/>
    <script src="/static/toastr/toastr.js"></script>

</head>
<input type="hidden" value="<?php echo htmlentities($voice['types']); ?>" id="voice">

<script>
    //语音播报
    function voice(name) {
        //判断语音是否开启
        if(<?php echo htmlentities($voice['status']); ?> === '0'){
            return false;
        }
        if($('#voice').val() === '思悦'){
            var audio= new Audio("/static/voice/siyue/"+name+".mp3");
        }else if($('#voice').val() === '若兮'){
            var audio= new Audio("/static/voice/ruoxi/"+name+".mp3");
        }else if($('#voice').val() === '艾琪'){
            var audio= new Audio("/static/voice/aiqi/"+name+".mp3");
        }else if($('#voice').val() === '艾美'){
            var audio= new Audio("/static/voice/aimei/"+name+".mp3");
        }else if($('#voice').val() === '艾悦'){
            var audio= new Audio("/static/voice/aiyue/"+name+".mp3");
        }else if($('#voice').val() === '青青'){
            var audio= new Audio("/static/voice/qingqing/"+name+".mp3");
        }else if($('#voice').val() === '翠姐'){
            var audio= new Audio("/static/voice/cuijie/"+name+".mp3");
        }else if($('#voice').val() === '姗姗'){
            var audio= new Audio("/static/voice/shanshan/"+name+".mp3");
        }else if($('#voice').val() === '小玥'){
            var audio= new Audio("/static/voice/xiaoyue/"+name+".mp3");
        }
        audio.play();//播放
    }
</script>
<!--<link href="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/3.4.0/css/bootstrap.css" rel="stylesheet">-->
<!--<link rel="stylesheet" href="/static/bootstrap/css/bootstrap.css">-->
<link href="/static/bootstrap3.0.css" rel="stylesheet" type="text/css"/>
<script src="/static/bootstrap/js/bootstrap.js"></script>
<link rel="stylesheet" href="/static/plug/animated-border/css/animated-border.css">
<link href="/static/plug/csshover/css/div.css" rel="stylesheet" type="text/css"/>

<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <body>
        <div class="x-nav">
            <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
                <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
            </a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15" id="app">
                <div class="layui-col-md12">
                    <div class="layui-card">

                        <div class="layui-card-body ">
                            <!--头部开始-->
                                <div class="layui-row">
                                    <div class="layui-col-md6">
                                        <blockquote class="layui-elem-quote layui-quote-nm">

                                            
<div class="layui-tab layui-tab-brief" lay-filter="docDemoTabBrief" style="margin-top: -18px;">
    <ul class="layui-tab-title">
        <li onclick="selects('')" <?php if(session('type_id') == 'all'): ?> class="layui-this" <?php endif; ?>>
        全部房型
        <form class="layui-form layui-col-space5" action="" method="get" id="layout_all">
            <div class="layui-input-inline layui-show-xs-block">
                <input type="hidden" name="type_id" value="all">
            </div>
        </form>
        </li>
        <?php if(is_array($layout) || $layout instanceof \think\Collection || $layout instanceof \think\Paginator): $i = 0; $__LIST__ = $layout;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$layout): $mod = ($i % 2 );++$i;?>
        <li onclick="selects(<?php echo htmlentities($layout['id']); ?>)" <?php if(session('type_id') == $layout['id']): ?> class="layui-this" <?php endif; ?>>
        <?php echo htmlentities($layout['type_name']); ?>
        <form class="layui-form layui-col-space5" action="" method="get" id="layout_<?php echo htmlentities($layout['id']); ?>">
            <div class="layui-input-inline layui-show-xs-block">
                <input type="hidden" name="type_id" value="<?php echo htmlentities($layout['id']); ?>">
            </div>
        </form>
        </li>
        <?php endforeach; endif; else: echo "" ;endif; ?>
    </ul>
    <div class="layui-tab-content"></div>
</div>


<div class="layui-tab layui-tab-brief" style="margin-top: -22px;">
    <ul class="layui-tab-title">
        <li onclick="room_status('')" <?php if(session('statuss') == 'all'): ?> class="layui-this" <?php endif; ?>>
            全部状态
            <form class="layui-form layui-col-space5" action="" method="get" id="type_id">
                <div class="layui-input-inline layui-show-xs-block">
                    <input type="hidden" name="status" value="all">
                </div>
            </form>
        </li>
        <li onclick="room_status('1')" <?php if(session('statuss') == '1'): ?> class="layui-this" <?php endif; ?>>
            空闲
            <form class="layui-form layui-col-space5" action="" method="get" id="type_id1" >
                <div class="layui-input-inline layui-show-xs-block">
                    <input type="hidden" name="status" value="1">
                </div>
            </form>
        </li>
        <li onclick="room_status('2')" <?php if(session('statuss') == '2'): ?> class="layui-this" <?php endif; ?>>
            在住
            <form class="layui-form layui-col-space5" action="" method="get" id="type_id2" >
                <div class="layui-input-inline layui-show-xs-block">
                    <input type="hidden" name="status" value="2">
                </div>
            </form>
        </li>
        <li onclick="room_status('4')" <?php if(session('statuss') == '4'): ?> class="layui-this" <?php endif; ?>>
            维修
            <form class="layui-form layui-col-space5" action="" method="get" id="type_id4" >
                <div class="layui-input-inline layui-show-xs-block">
                    <input type="hidden" name="status" value="4">
                </div>
            </form>
        </li>
        <li onclick="room_status('5')" <?php if(session('statuss') == '5'): ?> class="layui-this" <?php endif; ?>>
            锁房
            <form class="layui-form layui-col-space5" action="" method="get" id="type_id5" >
                <div class="layui-input-inline layui-show-xs-block">
                    <input type="hidden" name="status" value="5">
                </div>
            </form>
        </li>
        <li onclick="room_status('6')" <?php if(session('statuss') == '6'): ?> class="layui-this" <?php endif; ?>>
            脏房
            <form class="layui-form layui-col-space5" action="" method="get" id="type_id6" >
                <div class="layui-input-inline layui-show-xs-block">
                    <input type="hidden" name="status" value="6">
                </div>
            </form>
        </li>
    </ul>
    <div class="layui-tab-content"></div>
</div>



<!--<div class="layui-tab layui-tab-brief" style="margin-top: -22px;">
    <ul class="layui-tab-title">
        <li onclick="room_status('')" class="layui-this">
            全部状态
            <form class="layui-form layui-col-space5" action="" method="get" id="layout_all">
                <div class="layui-input-inline layui-show-xs-block">
                    <input type="hidden" name="status" value="all">
                </div>
            </form>
        </li>
        <li onclick="room_status('1')" >
            全天房
            <form class="layui-form layui-col-space5" action="" method="get" id="layout_all">
                <div class="layui-input-inline layui-show-xs-block">
                    <input type="hidden" name="status" value="1">
                </div>
            </form>
        </li>
        <li onclick="room_status('2')" >
            钟点房
            <form class="layui-form layui-col-space5" action="" method="get" id="layout_all">
                <div class="layui-input-inline layui-show-xs-block">
                    <input type="hidden" name="status" value="2">
                </div>
            </form>
        </li>
        <li onclick="room_status('4')" >
            今日预抵
            <form class="layui-form layui-col-space5" action="" method="get" id="layout_all">
                <div class="layui-input-inline layui-show-xs-block">
                    <input type="hidden" name="status" value="4">
                </div>
            </form>
        </li>
        <li onclick="room_status('5')" >
            今日离店
            <form class="layui-form layui-col-space5" action="" method="get" id="layout_all">
                <div class="layui-input-inline layui-show-xs-block">
                    <input type="hidden" name="status" value="5">
                </div>
            </form>
        </li>
    </ul>
    <div class="layui-tab-content"></div>
</div>-->



                                            <form class="layui-form layui-col-space5" action="" method="get" style="margin-top: -20px;">
                                                <div class="layui-input-inline layui-show-xs-block">
                                                    <input type="text" name="room_num" placeholder="请输入房间号" autocomplete="off" class="layui-input">
                                                </div>
                                                <div class="layui-input-inline layui-show-xs-block">
                                                    <button class="layui-btn" lay-submit="" lay-filter="sreach">
                                                        <i class="layui-icon">&#xe615;</i></button>
                                                </div>
                                            </form>
                                        </blockquote>
                                    </div>
                                    <div class="layui-col-md6">

                                        <blockquote class="layui-elem-quote layui-quote-nm">
                                            <span class="layui-badge">房间信息</span>
                                            <span id="more_personnel"></span>
                                            <table>
                                                <colgroup>
                                                    <col width="250">
                                                    <col width="120">
                                                </colgroup>
                                                <thead>
                                                <tr>
                                                    <th>房间号码：{{num}}</th>
                                                    <th>房间类型：{{type}}</th>
                                                    <th>房间楼层：{{storey}}</th>
                                                    <th>房间状态：{{status}}</th>
                                                </tr>
                                                <tr>
                                                    <th>姓名：{{guest_name}}</th>
                                                    <th>今日价格：{{monday}}</th>
                                                    <th>原始价格：{{price}}￥</th>
                                                    <th>定金：{{deposit}}￥</th>
                                                </tr>
                                                <tr>
                                                    <th>证据号码：{{credentials}}</th>
                                                    <th>每小时：{{hour}}￥</th>
                                                    <th>入住时间：{{in_time}}</th>
                                                    <th>离店时间：{{move_time}}</th>
                                                </tr>
                                                </thead>
                                            </table>

                                            <div id="add_xiaofei">
                                                <span class="layui-badge" >服务功能</span>
                                                <button type="button" class="layui-btn layui-btn-normal" @click="consume()">增加消费{{id}}</button>
                                                <button type="button" class="layui-btn layui-btn-normal" @click="emptys()">置净{{id}}</button>
                                                <button type="button" class="layui-btn layui-btn-normal" @click="report()">维修{{id}}</button>
                                                <button type="button" class="layui-btn layui-btn-normal" @click="lock_room()">锁房{{id}}</button>
                                            </div>

                                            <div id="tui" style="margin-top: 2px;">
                                                <span class="layui-badge layui-bg-blue">常用功能</span>
                                                <button type="button" class="layui-btn layui-btn-warm" @click="move()">房间入住{{id}}</button>
                                                <button type="button" class="layui-btn layui-btn-warm" @click="replace_room()">换房{{id}}</button>
                                            </div>

                                        </blockquote>

                                    </div>
                                </div>
                            <!--头部结束-->
                            <div class="layui-row">

                                <div class="Service-box">
                                    <div class="Service-content clearfix">

                                        <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                                        <a href="javascript:;" class="Service-item" style="margin: 10px;" @click="action(<?php echo htmlentities($vo['id']); ?>)">
                                            <?php if($vo['room_id'] == 'no' && $vo['status'] == '2'): ?><span class="layui-badge"><?php echo htmlentities($vo['id']); ?>-主</span>
                                            <?php elseif($vo['status'] == '2'): ?><span class="layui-badge"><?php echo htmlentities($vo['room_id']); ?>-次</span>
                                            <?php endif; ?>
                                            <h3 class="item-title"><?php echo htmlentities($vo['room_num']); ?></h3>
                                            <?php if($vo['status'] == '1'): ?><span class="layui-badge layui-bg-blue">空闲</span>
                                            <?php elseif($vo['status'] == '2'): ?><span class="layui-badge"><?php echo htmlentities($vo['guest_name']); ?></span>
                                            <?php elseif($vo['status'] == '3'): ?><span class="layui-badge">vip</span>
                                            <?php elseif($vo['status'] == '4'): ?><span class="layui-badge">维修</span>
                                            <?php elseif($vo['status'] == '5'): ?><span class="layui-badge">锁房</span>
                                            <?php elseif($vo['status'] == '6'): ?><span class="layui-blue">待打扫</span>
                                            <?php else: ?><span class="layui-badge">预订</span>
                                            <?php endif; ?>
                                            <div class="item-text"><?php echo htmlentities($vo['type_name']); ?></div>
                                            <div class="item-text">今日价格:<?php echo htmlentities($vo['monday']); ?></div>
                                        </a>
                                        <?php endforeach; endif; else: echo "" ;endif; ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <?php echo $list; ?>
                </div>
            </div>
        </div>
    </body>

<script>
    layui.use(['laydate', 'form'],
        function() {
            var laydate = layui.laydate;

            //执行一个laydate实例
            laydate.render({
                elem: '#start' //指定元素
            });

            //执行一个laydate实例
            laydate.render({
                elem: '#end' //指定元素
            });
        });
    new 	Vue({
        el:'#app',
        data:{
            id:'',
            num:'',
            type:'',
            status:'',
            price:'',
            deposit:'',
            guest_name:'',
            monday:'',
            storey:'',
            hour:'',
            credentials:'',
            in_time:'',
            move_time:'',
            styles:''
        },
        //点击房间后的操作
        methods:{
            action:function(id){
                _this = this;
                $.ajax({
                    type:"post",
                    url: "<?php echo url('home/welcome/index'); ?>",
                    data: {id:id,},
                    success: function(data){
                        console.log(data);
                        _this.num = data.room_num;
                        _this.type = data.type_name;
                        _this.status = data.status;
                        _this.price = data.price;
                        _this.deposit = data.deposit;
                        _this.id = data.id;
                        _this.guest_name = data.guest_name;
                        _this.monday = data.monday;
                        _this.storey = data.storey;
                        _this.hour = data.hour;
                        _this.credentials = data.credentials;
                        _this.in_time = data.in_time;
                        _this.move_time = data.move_time;
                        if(data.room_id === 'no' && data.status === '2'){
                            $('#tui_del').remove();
                            var text = "  <button type=\"button\" id=\"tui_del\" class=\"layui-btn layui-btn-warm\" onclick=\"xadmin.open('退房','/home/atrial/out_room/id/"+data.id+"',700,800)\">\n" +
                                "           退房\n" +
                                "         </button> ";
                            $('#tui').append(text);
                        }else if(data.status === '2'){
                            $('#tui_del').remove();
                            var text = " <button type=\"button\" id=\"tui_del\" class=\"layui-btn layui-btn-warm\" onclick=\"out_room("+data.id+")\">退房</button> ";
                            $('#tui').append(text);
                        }
                        if(data.status === '2'){
                            $('#personnel').remove();
                            var text = "  <button type=\"button\" id=\"personnel\" class=\"layui-btn layui-btn-normal\" onclick=\"xadmin.open('更多入住人员','/home/welcome/more_personnel/room_num/"+data.room_num+"',800,600)\">\n" +
                                "           在住人员\n" +
                                "         </button> ";
                            $('#more_personnel').append(text);
                            /*租借物品*/
                            $('#xiaofei').remove();
                            var text = "  <button type=\"button\" id=\"xiaofei\" class=\"layui-btn layui-btn-normal\" onclick=\"xadmin.open('租借物品','/home/lease/add_rental/id/"+data.id+"',450,300)\">\n" +
                                "           租借物品\n" +
                                "         </button> ";
                            $('#add_xiaofei').append(text);
                        }

                    }});
            },//跳到入住的界面
            move:function () {
                if(this.id === ''){
                    toastr.error('请选择房间');
                    return false;
                }
                window.location.href="/home/welcome/move/id/"+this.id;
            },//更换房间
            replace_room:function(){
                if(this.id === ''){
                    toastr.error('请选择房间');
                    return false;
                }
                window.location.href="/home/atrial/replace_room/id/"+this.id;
            },//增加消费
            consume:function(){
                if(this.id === ''){
                    toastr.error('请选择房间');
                    return false;
                }
                window.location.href="/home/atrial/consume/id/"+this.id;

            },//置空房间
            emptys:function () {
                if(this.id === ''){
                    toastr.error('请选择房间');
                    return false;
                }
                _this = this;
                $.ajax({
                    type:"post",
                    url: "<?php echo url('home/atrial/emptys'); ?>",
                    data: {id:this.id,},
                    success: function(data){
                        console.log(data);
                        toastr.error(data.msg);
                        if(data.code == 100){
                            voice('eliminate');
                            setTimeout(function () {
                                layer.closeAll();
                                location.reload();
                            },1500);
                        }
                    }});
            },//维修
            report:function () {
                if(this.id === ''){
                    toastr.error('请选择房间');
                    return false;
                }
                _this = this;
                $.ajax({
                    type:"post",
                    url: "<?php echo url('home/atrial/report'); ?>",
                    data: {id:this.id,},
                    success: function(data){
                        console.log(data);
                        toastr.error(data.msg);
                        if(data.code == 100){
                            voice('fault');
                            setTimeout(function () {
                                layer.closeAll();
                                location.reload();
                            },1500);
                        }
                    }});
            },//锁房
            lock_room:function () {
                if(this.id === ''){
                    toastr.error('请选择房间');
                    return false;
                }
                _this = this;
                $.ajax({
                    type:"post",
                    url: "<?php echo url('home/atrial/lock_room'); ?>",
                    data: {id:this.id,},
                    success: function(data){
                        console.log(data);
                        toastr.error(data.msg);
                        if(data.code == 100){
                            voice('lock');
                            setTimeout(function () {
                                layer.closeAll();
                                location.reload();
                            },1500);
                        }
                    }});
            },
        }
    })
    //次房间退房
    function out_room(id) {
        _this = this;
        $.ajax({
            type:"post",
            url: "<?php echo url('home/atrial/out_room'); ?>",
            data: {id:id,},
            success: function(data){
                console.log(data);
                toastr.error(data.msg);
                if(data.code == 100){
                    voice('return');
                    setTimeout(function () {
                        layer.closeAll();
                        location.reload();
                    },1500);
                }
            }});
    }
    //查询房型
    function selects(id) {
        if(id === ''){
            $('#layout_all').submit();
        }else{
            $('#layout_'+id+'').submit();
        }
    }
    //查询房间状态
    function room_status(id) {
        if(id === ''){
            $('#type_id').submit();
        }else{
            $('#type_id'+id+'').submit();
        }
    }
</script>


</html>