<?php /*a:3:{s:53:"D:\phpstudy_pro\WWW\tp\view\home\atrial\out_room.html";i:1604890778;s:51:"D:\phpstudy_pro\WWW\tp\view\home\common\static.html";i:1603931011;s:54:"D:\phpstudy_pro\WWW\tp\view\home\common\resources.html";i:1603609812;}*/ ?>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
    <meta charset="UTF-8">
    <title><?php echo htmlentities($system['hotel_name']); ?>(多酒店版)</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="stylesheet" href="/static/admin/css/font.css">
    <link rel="stylesheet" href="/static/admin/css/xadmin.css">
    <script src="/static/admin/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="/static/admin/js/xadmin.js"></script>

    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/2.0.3/jquery.js"></script>
    <script src="/static/jquery.printarea.js"></script>

    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


    <link href="/static/toastr/toastr.css" rel="stylesheet"/>
    <script src="/static/toastr/toastr.js"></script>

</head>
<input type="hidden" value="<?php echo htmlentities($voice['types']); ?>" id="voice">

<script>
    //语音播报
    function voice(name) {
        //判断语音是否开启
        if(<?php echo htmlentities($voice['status']); ?> === '0'){
            return false;
        }
        if($('#voice').val() === '思悦'){
            var audio= new Audio("/static/voice/siyue/"+name+".mp3");
        }else if($('#voice').val() === '若兮'){
            var audio= new Audio("/static/voice/ruoxi/"+name+".mp3");
        }else if($('#voice').val() === '艾琪'){
            var audio= new Audio("/static/voice/aiqi/"+name+".mp3");
        }else if($('#voice').val() === '艾美'){
            var audio= new Audio("/static/voice/aimei/"+name+".mp3");
        }else if($('#voice').val() === '艾悦'){
            var audio= new Audio("/static/voice/aiyue/"+name+".mp3");
        }else if($('#voice').val() === '青青'){
            var audio= new Audio("/static/voice/qingqing/"+name+".mp3");
        }else if($('#voice').val() === '翠姐'){
            var audio= new Audio("/static/voice/cuijie/"+name+".mp3");
        }else if($('#voice').val() === '姗姗'){
            var audio= new Audio("/static/voice/shanshan/"+name+".mp3");
        }else if($('#voice').val() === '小玥'){
            var audio= new Audio("/static/voice/xiaoyue/"+name+".mp3");
        }
        audio.play();//播放
    }
</script>
<!--<link href="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/3.4.0/css/bootstrap.css" rel="stylesheet">-->
<!--<link rel="stylesheet" href="/static/bootstrap/css/bootstrap.css">-->
<link href="/static/bootstrap3.0.css" rel="stylesheet" type="text/css"/>
<script src="/static/bootstrap/js/bootstrap.js"></script>
<style>

</style>
    <body>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-col-md12">
                            <div class="layui-card">
                                <div class="layui-card-header">入住信息</div>
                                <div class="layui-card-body ">
                                    <table class="layui-table" lay-skin="row" lay-size="sm">
                                        <tbody>
                                        <tr>
                                            <th>入住时间</th>
                                            <td>
                                                    <div class="layui-form layui-form-item">
                                                        <div class="layui-input-inline">
                                                            <input type="text" required="" lay-verify="required" value="<?php echo htmlentities($list['in_time']); ?>"
                                                                   autocomplete="off" class="layui-input" readonly="readonly">
                                                        </div>
                                                        <div class="layui-input-inline">
                                                            <input type="text" required="" lay-verify="required" value="<?php echo htmlentities($list['move_time']); ?>"
                                                                   autocomplete="off" class="layui-input" readonly="readonly">
                                                        </div>
                                                    </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>入住房间</th>
                                            <td>
                                                <div class="layui-form layui-form-item">
                                                    <div class="layui-input-inline">
                                                        <input type="text" required="" lay-verify="required" value="<?php echo htmlentities($list['num_arr']); ?>"
                                                               autocomplete="off" class="layui-input" readonly="readonly">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>房间数量</th>
                                            <td>
                                                <div class="layui-form layui-form-item">
                                                    <div class="layui-input-inline">
                                                        <input type="text" required="" lay-verify="required" value="<?php echo htmlentities($list['count']); ?>"
                                                               autocomplete="off" class="layui-input" readonly="readonly">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>明细</th>
                                            <td>
                                                <ul class="layui-row layui-col-space12 layui-this x-admin-carousel x-admin-backlog">
                                                    <li style="width: 100px; float: left">
                                                        <div href="javascript:;" class="x-admin-backlog-body">
                                                            <h3>总金额</h3>
                                                            <p>
                                                                <cite><?php echo htmlentities($list['income_details']); ?></cite>
                                                            </p>
                                                        </div>
                                                    </li>
                                                    <li style="width: 100px;float: left"">
                                                        <div href="javascript:;" class="x-admin-backlog-body">
                                                            <h3>总定金</h3>
                                                            <p>
                                                                <cite><?php echo htmlentities($list['deposit_record']); ?></cite>
                                                            </p>
                                                        </div>
                                                    </li>
                                                    <li style="width: 100px;float: left"">
                                                        <div href="javascript:;" class="x-admin-backlog-body">
                                                            <h3>活动折扣</h3>
                                                            <p>
                                                                <cite><?php echo htmlentities($list['activity_price']); ?></cite>
                                                            </p>
                                                        </div>
                                                    </li>
                                                    <li style="width: 100px;float: left"">
                                                        <div href="javascript:;" class="x-admin-backlog-body">
                                                            <h3>会员折扣</h3>
                                                            <p>
                                                                <cite><?php echo htmlentities($list['member_price']); ?></cite>
                                                            </p>
                                                        </div>
                                                    </li>
                                                    <li style="width: 100px;float: left"">
                                                        <div href="javascript:;" class="x-admin-backlog-body">
                                                            <h3>实付房费</h3>
                                                            <p>
                                                                <cite><?php echo htmlentities($list['price_receivable']); ?></cite>
                                                            </p>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="layui-col-md12">
                            <div class="layui-card">
                                <div class="layui-card-header">
                                    退款
                                    <div style="float: right;">
                                        <button type="button" class="layui-btn" onclick="current()">当前房间退房</button>
                                        <button type="button" class="layui-btn" onclick="alls()">所有房间退房</button>
                                        <button type="button" class="layui-btn">打印数据</button>
                                    </div>

                                </div>
                                <div class="layui-card-body ">
                                    <table class="layui-table" lay-skin="row" lay-size="sm">
                                        <tbody>
                                        <tr>
                                            <th>房费退款</th>
                                            <td>
                                                <div class="layui-form layui-form-item">
                                                    <div class="layui-input-inline">
                                                        <input type="text" id="room_rate" required="" lay-verify="required" value="0"
                                                               autocomplete="off" class="layui-input">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>押金退款</th>
                                            <td>
                                                <div class="layui-form layui-form-item">
                                                    <div class="layui-input-inline">
                                                        <input type="text"  id="deposit" required="" lay-verify="required" value="0"
                                                               autocomplete="off" class="layui-input">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>退款理由</th>
                                            <td>
                                                <div class="layui-form layui-form-item">
                                                    <div class="layui-input-inline">
                                                        <input type="text" id="reason" required="" lay-verify="required"
                                                               autocomplete="off" class="layui-input">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>备注</th>
                                            <td>
                                                <div class="layui-form layui-form-item">
                                                    <textarea name="desc" id="desc" placeholder="请输入内容" class="layui-textarea"></textarea>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </body>

<script>
    //当前房间退款
    function current() {
        $.ajax({
            type:"post",
            url: "<?php echo url('home/atrial/out_room'); ?>",
            data: {
                id:<?php echo htmlentities($id); ?>,
                room_rate:$('#room_rate').val(),
                deposit:$('#deposit').val(),
                reason:$('#reason').val(),
                desc:$('#desc').val()
            },
            success: function(data){
                toastr.error(data.msg);
                if(data.code == 100){
                    voice('return');
                    setTimeout(function () {
                        layer.closeAll();
                        parent.location.reload();
                    },1500);
                }
            }});
    }
    //所有房间退款
    function alls() {
        $.ajax({
            type:"post",
            url: "<?php echo url('home/atrial/out_room'); ?>",
            data: {
                id:<?php echo htmlentities($id); ?>,
                room_rate:$('#room_rate').val(),
                deposit:$('#deposit').val(),
                reason:$('#reason').val(),
                desc:$('#desc').val(),
                all:'1'
            },
            success: function(data){
                toastr.error(data.msg);
                if(data.code == 100){
                    voice('return');
                    setTimeout(function () {
                        layer.closeAll();
                        parent.location.reload();
                    },1500);
                }
            }});
    }

</script>


</html>