<?php /*a:2:{s:49:"G:\phpstudy_pro\WWW\tp\view\home\index\index.html";i:1605237930;s:51:"G:\phpstudy_pro\WWW\tp\view\home\common\static.html";i:1603931011;}*/ ?>
<!doctype html>
<html class="x-admin-sm">
    <head>
        <head>
    <meta charset="UTF-8">
    <title><?php echo htmlentities($system['hotel_name']); ?>(多酒店版)</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="stylesheet" href="/static/admin/css/font.css">
    <link rel="stylesheet" href="/static/admin/css/xadmin.css">
    <script src="/static/admin/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="/static/admin/js/xadmin.js"></script>

    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/2.0.3/jquery.js"></script>
    <script src="/static/jquery.printarea.js"></script>

    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


    <link href="/static/toastr/toastr.css" rel="stylesheet"/>
    <script src="/static/toastr/toastr.js"></script>

</head>
<input type="hidden" value="<?php echo htmlentities($voice['types']); ?>" id="voice">

<script>
    //语音播报
    function voice(name) {
        //判断语音是否开启
        if(<?php echo htmlentities($voice['status']); ?> === '0'){
            return false;
        }
        if($('#voice').val() === '思悦'){
            var audio= new Audio("/static/voice/siyue/"+name+".mp3");
        }else if($('#voice').val() === '若兮'){
            var audio= new Audio("/static/voice/ruoxi/"+name+".mp3");
        }else if($('#voice').val() === '艾琪'){
            var audio= new Audio("/static/voice/aiqi/"+name+".mp3");
        }else if($('#voice').val() === '艾美'){
            var audio= new Audio("/static/voice/aimei/"+name+".mp3");
        }else if($('#voice').val() === '艾悦'){
            var audio= new Audio("/static/voice/aiyue/"+name+".mp3");
        }else if($('#voice').val() === '青青'){
            var audio= new Audio("/static/voice/qingqing/"+name+".mp3");
        }else if($('#voice').val() === '翠姐'){
            var audio= new Audio("/static/voice/cuijie/"+name+".mp3");
        }else if($('#voice').val() === '姗姗'){
            var audio= new Audio("/static/voice/shanshan/"+name+".mp3");
        }else if($('#voice').val() === '小玥'){
            var audio= new Audio("/static/voice/xiaoyue/"+name+".mp3");
        }
        audio.play();//播放
    }
</script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script>
            // 是否开启刷新记忆tab功能
            // var is_remember = false;
        </script>
    </head>
    <body class="index">
        <!-- 顶部开始 -->
        <div class="container">
            <div class="logo">
                <a href="/static/admin/index.html"><?php echo htmlentities($system['hotel_name']); ?>(多酒店版)</a></div>
            <div class="left_open">
                <a><i title="展开左侧栏" class="iconfont">&#xe699;</i></a>
            </div>

            <ul class="layui-nav right" lay-filter="">
                <a href="javascript:;"><?php echo session('classe');; ?></a>
                <li class="layui-nav-item">
                    <a href="javascript:;"><?php echo session('admin_id');; ?><?php echo session('admin');; ?></a>
                    <dl class="layui-nav-child">
                        <!-- 二级菜单 -->
                        <dd>
                            <a onclick="xadmin.open('个人信息','http://www.baidu.com')">个人信息</a></dd>
                        <dd>
                            <a onclick="xadmin.open('切换帐号','http://www.baidu.com')">切换帐号</a></dd>
                        <dd>
                            <a href="<?php echo url('home/login/logout'); ?>">退出</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item to-index">
                    <a href="/"><?php echo htmlentities($list['building']); ?></a>
                </li>
            </ul>
        </div>
        <!-- 顶部结束 -->
        <!-- 中部开始 -->
        <!-- 左侧菜单开始 -->
        <div class="left-nav">
            <div id="side-nav">
                <ul id="nav">
                    <li>
                        <a onclick="xadmin.add_tab('房间动态','/home/welcome/index')">
                            <i class="iconfont left-nav-li" lay-tips="登记入住">&#xe722;</i>
                            <cite>房间动态</cite>
                            <i class="iconfont nav_right">&#xe697;</i></a>
                    </li>

                    <!-- <li>
                        <a onclick="xadmin.add_tab('房间信息','/home/index/welcome')">
                            <i class="iconfont left-nav-li" lay-tips="登记入住">&#xe6b8;</i>
                            <cite>房间信息</cite>
                            <i class="iconfont nav_right">&#xe697;</i></a>
                    </li> -->

                    <li>
                        <a href="javascript:;">
                            <i class="iconfont left-nav-li" lay-tips="房间管理">&#xe698;</i>
                            <cite>房间管理</cite>
                            <i class="iconfont nav_right">&#xe697;</i></a>
                        <ul class="sub-menu">

                            <li>
                                <a onclick="xadmin.add_tab('楼层设置','/home/storeys/index')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>楼层设置</cite></a>
                            </li>
                            <li>
                                <a onclick="xadmin.add_tab('房型设置','/index/layouts/index')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>房型设置</cite></a>
                            </li>
                            <li>
                                <a onclick="xadmin.add_tab('房间设置','/home/rooms/index')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>房间设置</cite></a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href="javascript:;">
                            <i class="iconfont left-nav-li" lay-tips="优惠活动">&#xe702;</i>
                            <cite>优惠活动</cite>
                            <i class="iconfont nav_right">&#xe697;</i></a>
                        <ul class="sub-menu">
                            <li>
                                <a onclick="xadmin.add_tab('优惠活动','/index/activity/index')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>优惠活动</cite></a>
                            </li>
                        </ul>
                        <ul class="sub-menu">
                            <li>
                                <a onclick="xadmin.add_tab('房间优惠','/index/pricesystem/index')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>房间优惠</cite></a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a onclick="xadmin.add_tab('预定信息','/home/subscribe/index')">
                            <i class="iconfont left-nav-li" lay-tips="登记入住">&#xe74e;</i>
                            <cite>预定信息</cite>
                            <i class="iconfont nav_right">&#xe697;</i></a>
                    </li>

                    <li>
                        <a href="javascript:;">
                            <i class="iconfont left-nav-li" lay-tips="商品管理">&#xe698;</i>
                            <cite>商品管理</cite>
                            <i class="iconfont nav_right">&#xe697;</i></a>
                        <ul class="sub-menu">
                            <li>
                                <a onclick="xadmin.add_tab('商品模型','/home/goods/index')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>商品模型</cite></a>
                            </li>
                            <li>
                                <a onclick="xadmin.add_tab('采购商品','/home/purchase/index')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>采购商品</cite></a>
                            </li>
<!--                            <li>
                                <a onclick="xadmin.add_tab('商品入库','/home/purchase/warehousing')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>商品入库</cite></a>
                            </li>-->
                            <li>
                                <a onclick="xadmin.add_tab('查看所有商品','/home/goods/select_goods')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>查看所有商品</cite></a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href="javascript:;">
                            <i class="iconfont left-nav-li" lay-tips="营业查询">&#xe6ac;</i>
                            <cite>营业查询</cite>
                            <i class="iconfont nav_right">&#xe697;</i></a>
                        <ul class="sub-menu">
                            <li>
                                <a onclick="xadmin.add_tab('入住记录','/home/income/index')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>入住记录</cite></a>
                            </li>
                            <li>
                                <a onclick="xadmin.add_tab('退房记录','/home/income/room_return')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>退房记录</cite></a>
                            </li>
                            <li>
                                <a onclick="xadmin.add_tab('预订记录','/home/income/subscribe')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>预订记录</cite></a>
                            </li>
                            <li>
                                <a onclick="xadmin.add_tab('换房记录','/home/income/upd_rooms')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>换房记录</cite></a>
                            </li>
                            <li>
                                <a onclick="xadmin.add_tab('会员充值记录','/home/member/recharge_record')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>会员充值记录</cite></a>
                            </li>
                            <li>
                                <a onclick="xadmin.add_tab('会员消费记录','/home/member/index')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>会员消费记录</cite></a>
                            </li>
                            <li>
                                <a onclick="xadmin.add_tab('商品消费记录','/home/income/goods_shop')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>商品消费记录</cite></a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href="javascript:;">
                            <i class="iconfont left-nav-li" lay-tips="交班管理">&#xe82a;</i>
                            <cite>交班管理</cite>
                            <i class="iconfont nav_right">&#xe697;</i></a>
                        <ul class="sub-menu">
                            <li>
                                <a onclick="xadmin.add_tab('交班管理','/home/handover/index')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>交班管理</cite></a>
                            </li>
                            <li>
                                <a onclick="xadmin.add_tab('交班记录','/home/handover/record')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>交班记录</cite></a>
                            </li>

                        </ul>
                    </li>

                    <li>
                        <a href="javascript:;">
                            <i class="iconfont left-nav-li" lay-tips="会员营销">&#xe6b8;</i>
                            <cite>会员营销</cite>
                            <i class="iconfont nav_right">&#xe697;</i></a>
                        <ul class="sub-menu">
                            <li>
                                <a onclick="xadmin.add_tab('会员类型','/home/member/viptype')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>会员类型</cite></a>
                            </li>
                            <li>
                                <a onclick="xadmin.add_tab('会员资料','/home/member/index')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>会员资料</cite></a>
                            </li>
<!--                            <li>
                                <a onclick="xadmin.add_tab('充值优惠','/home/recharge/index')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>充值优惠</cite></a>
                            </li>-->
                            <li>
                                <a onclick="xadmin.add_tab('黑名单管理','/home/blacklist/index')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>黑名单管理</cite></a>
                            </li>
                        </ul>
                    </li>


                    <li>
                        <a onclick="xadmin.add_tab('查看房价','/home/houseprice/index')">
                            <i class="iconfont left-nav-li" lay-tips="查看房价">&#xe699;</i>
                            <cite>查看房价</cite>
                            <i class="iconfont nav_right">&#xe697;</i></a>
                    </li>

                    <li>
                        <a onclick="xadmin.add_tab('数据统计','/home/statistic/index')">
                            <i class="iconfont left-nav-li" lay-tips="数据统计">&#xe699;</i>
                            <cite>数据统计</cite>
                            <i class="iconfont nav_right">&#xe697;</i>
                        </a>
                    </li>


                    <li>
                        <a href="javascript:;">
                            <i class="iconfont left-nav-li" lay-tips="客房服务">&#xe725;</i>
                            <cite>客房服务</cite>
                            <i class="iconfont nav_right">&#xe697;</i></a>
                        <ul class="sub-menu">
                            <li>
                                <a onclick="xadmin.add_tab('服务管理','/home/roomservice/index')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>服务管理</cite></a>
                            </li>
                            <li>
                                <a onclick="xadmin.add_tab('人员管理','/home/roomservice/personnel')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>人员管理</cite></a>
                            </li>
                            <li>
                                <a onclick="xadmin.add_tab('派发任务','/home/roomservice/room_publish')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>派发任务</cite></a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href="javascript:;">
                            <i class="iconfont left-nav-li" lay-tips="物品租借">&#xe6eb;</i>
                            <cite>物品租借</cite>
                            <i class="iconfont nav_right">&#xe697;</i></a>
                        <ul class="sub-menu">
                            <li>
                                <a onclick="xadmin.add_tab('物品管理','/home/lease/index')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>物品管理</cite></a>
                            </li>
                            <li>
                                <a onclick="xadmin.add_tab('租借记录','/home/lease/rental_records')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>租借记录</cite></a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href="javascript:;">
                            <i class="iconfont left-nav-li" lay-tips="夜审设置">&#xe6a9;</i>
                            <cite>夜审设置</cite>
                            <i class="iconfont nav_right">&#xe697;</i></a>
                        <ul class="sub-menu">
                            <li>
                                <a onclick="xadmin.add_tab('夜审设置','/home/nightaudit/index')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>夜审设置</cite></a>
                            </li>
                            <li>
                                <a onclick="xadmin.add_tab('应到未到订单','/home/nightaudit/future')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>应到未到订单</cite></a>
                            </li>
                            <li>
                                <a onclick="xadmin.add_tab('应离未离订单','/home/nightaudit/index')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>应离未离订单</cite></a>
                            </li>

                            <li>
                                <a onclick="xadmin.add_tab(' 夜审记录','/index/Pricesystem/index')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>夜审记录</cite></a>
                            </li>

                        </ul>
                    </li>

                    <li>
                        <a href="javascript:;">
                            <i class="iconfont left-nav-li" lay-tips="系统设置">&#xe6eb;</i>
                            <cite>系统设置</cite>
                            <i class="iconfont nav_right">&#xe697;</i></a>
                        <ul class="sub-menu">
                            <li>
                                <a onclick="xadmin.add_tab('酒店设置','/home/systems/basic')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>酒店设置</cite></a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="iconfont">&#xe70b;</i>
                                    <cite>计费方式</cite>
                                    <i class="iconfont nav_right">&#xe697;</i></a>
                                <ul class="sub-menu">
                                    <li>
                                        <a onclick="xadmin.add_tab('普通用户','/home/charges/index')">
                                            <i class="iconfont">&#xe6a7;</i>
                                            <cite>普通用户</cite></a>
                                    </li>
                                    <li>
                                        <a onclick="xadmin.add_tab('酒店会员','/home/charges/vip')">
                                            <i class="iconfont">&#xe6a7;</i>
                                            <cite>酒店会员</cite></a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a onclick="xadmin.add_tab('班次设置','/home/classe/index')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>班次设置</cite></a>
                            </li>
                            <li>
                                <a onclick="xadmin.add_tab('短信设置','/apply/message/index')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>短信设置</cite></a>
                            </li>

                            <?php if(file_exists($file)): ?>
                            <li>
                                <a onclick="xadmin.add_tab('语音设置','/apply/voice/index')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>语音设置</cite></a>
                            </li>
                            <?php endif; ?>
                        </ul>
                    </li>
                    <input type="hidden" value="<?php echo htmlentities($list['building_id']); ?>" id="building_id">

                </ul>
            </div>
        </div>
        <!-- <div class="x-slide_left"></div> -->
        <!-- 左侧菜单结束 -->
        <!-- 右侧主体开始 -->
        <div class="page-content">
            <div class="layui-tab tab" lay-filter="xbs_tab" lay-allowclose="false">
                <ul class="layui-tab-title">
                    <li class="home">
                        <i class="layui-icon">&#xe68e;</i>我的桌面</li></ul>
                <div class="layui-unselect layui-form-select layui-form-selected" id="tab_right">
                    <dl>
                        <dd data-type="this">关闭当前</dd>
                        <dd data-type="other">关闭其它</dd>
                        <dd data-type="all">关闭全部</dd></dl>
                </div>
                <div class="layui-tab-content">
                    <div class="layui-tab-item layui-show">
                        <iframe src="<?php echo url('home/welcome/index'); ?>" frameborder="0" scrolling="yes" class="x-iframe"></iframe>
                    </div>
                </div>
                <div id="tab_show"></div>
            </div>
        </div>
        <div class="page-content-bg"></div>
        <style id="theme_style"></style>
        <!-- 右侧主体结束 -->
        <!-- 中部结束 -->
    </body>

</html>
<script>
    // 假设服务端ip为127.0.0.1
    ws = new WebSocket("ws://127.0.0.1:8282");
    ws.onopen = function() {
        setTimeout(function () {

            //右下弹出
            layer.open({
                type: 1
                ,offset: 'rb'
                ,content: '<div style="padding: 20px 80px;">欢迎使用<?php echo htmlentities($system['hotel_name']); ?>管理系统</div>'
                ,btn: '关闭全部'
                ,btnAlign: 'c' //按钮居中
                ,shade: 0 //不显示遮罩
                ,anim: 2
                ,time: 4000
                ,yes: function(){
                    layer.closeAll();
                }
            });
            //调用语音
            voice('welcome');
        },1000);
    };
    ws.onmessage = function(e) {
        console.log("收到服务端的消息：" + e.data);
        var result = $.parseJSON(e.data);
        if(result.count > 0 && result.list[0].building_id  === $('#building_id').val()){
            console.log(result.list[0].building_id);
            var arrs = [];
            for (var i=0;i < result.count;i++)
            {
                arrs.push(result.list[i].room_num)
            }
            console.log(arrs.join("-"));
            //右下弹出
            layer.open({
                type: 1
                ,offset: 'rb'
                ,content: "<div style='padding: 20px 80px;'>你要已经超时的房间<br>"+arrs.join(",")+"</div>"
                ,btn: '关闭全部'
                ,btnAlign: 'c' //按钮居中
                ,shade: 0 //不显示遮罩
                ,anim: 2
                ,time: 4000
                ,yes: function(){
                    layer.closeAll();
                }
            });

        }

    };

    function send_sms(){
        ws.send(<?php echo session('building_id');; ?>);
    }
    //重复执行某个方法
    var t1 = window.setInterval(send_sms,6000);

</script>

</script>