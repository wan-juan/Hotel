<?php
namespace app\home\controller;

use app\index\controller\Basics;
use think\facade\Db;

class Member extends Basics
{
    /*
     * 会员 首页
     * */
    public function index()
    {
        if(request()->isGet()){
            $data = input('param.');
            if(!empty($data['start']) && !empty($data['end'])){
                $data['start'] = strtotime($data['start']);
                $data['end'] = strtotime($data['end']);
                $map = [
                    ['a.building_id','=',session('building_id')],
                    ['a.create_time','>',$data['start']],
                    ['a.create_time','<',$data['end']]
                ];
            }
            if(!empty($data['type'])){
                $map = [
                    ['a.building_id','=',session('building_id')],
                    ['a.type','=',$data['type']],
                ];
            }
            if(!empty($data['name'])){
                $name = '%'.$data['name']."%";
                $map = [
                    ['a.building_id','=',session('building_id')],
                    ['a.name','like',$name],
                ];
            }
        }
        if(!isset($map)){
            $map = [
                ['a.building_id','=',session('building_id')]
            ];
        }
        $list =  Db::table('member')
            ->alias('a')
            ->field('a.*,b.vipname')
            ->join('viptype b','a.type = b.id')
            ->where($map)
            ->paginate(['list_rows'=> 15,'query' => input('param.')]);

        $viptype = Db::table('viptype')->where('building_id',session('building_id'))->select();
        return view('index',['list' => $list,'viptype'=>$viptype]);
    }

    /*
     * 会员 添加
     * */
    public function vip()
    {
        if(request()->isAjax()){
            $data = input('param.');
            $data['create_time'] = time();
            $data['building_id'] = session('building_id');
            //判断是否添加成功
            if(Db::name('member')->insert($data)){
                return $this->return_json('新增成功','100');
            }else{
                return $this->return_json('新增失败','0');
            }
        }
//        $list = $this->select_all('viptype');
        $list = Db::table('viptype')->where('building_id',session('building_id'))->select();
        return view('vip',['list' => $list]);
    }

    /*
     * 会员 编辑
     * */
    public function data_edit()
    {
        if(request()->isAjax()){
            $data = input('param.');

            if(Db::name('member')->update($data)){
                return $this->return_json('编辑成功','100');
            }else{
                return $this->return_json('编辑失败','0');
            }
        }
        $member = Db::name('member')->where('id',input('id'))->find();
        $viptype = Db::table('viptype')->where('id',$member['type'])->find();
        $list = Db::table('viptype')->where('building_id',session('building_id'))->select();
        return view('data_edit',['list' => $list,'member'=>$member,'viptype'=>$viptype]);
    }

    /*
     * 会员 删除
     * */
    public function delete()
    {
        if(request()->isAjax()){
            $data = input('id');
            if( Db::table('member')->delete($data)){
                return $this->return_json('删除成功','100');
            }else{
                return $this->return_json('删除失败','0');
            }
        }
    }

    /*
     * 会员充值
     * */
    public function recharge(){

        if(request()->isAjax()){
            $data = input('param.');
            //查询原来的余额
            $res = Db::table('member')->where('id',$data['id'])->find();
            //查询充值优惠
/*            $list = Db::table('recharge')->where('building_id',session('building_id'))->select();
            foreach ($list as $value){
                if($data['money'] >= $value['recharge']){
                    $money =$res['money'] + $data['money'] + $value['give'];
                    echo '进来了|'.$value['recharge'];
                    dump($money);
                }
            }*/

            $data['money'] =$res['money'] + $data['money'];
            if( Db::table('member')->update($data)){
                Db::table('home_member_record')->insert([
                    'vip_id'=> $data['id'],
                    'money'=> $data['money'],
                    'create_time'=> time(),
                    'building_id'=>session('building_id'),
                    'operator' =>session('admin_id')
                ]);
                return $this->return_json('充值成功','100');
            }else{
                return $this->return_json('充值失败','0');
            }
        }
        return view('recharge',['id'=>input('id')]);
    }

    /*
     * 会员充值记录
     * */
    public function recharge_record()
    {
        if(request()->isGet()){
            $data = input('param.');
            if(!empty($data['start']) && !empty($data['end'])){
                $data['start'] = strtotime($data['start']);
                $data['end'] = strtotime($data['end']);
                $map = [
                    ['a.building_id','=',session('building_id')],
                    ['a.create_time','>=',$data['start']],
                    ['a.create_time','<=',$data['end']]
                ];
            }
            if(!empty($data['name'])){
                $name = '%'.$data['name']."%";
                $map = [
                    ['a.building_id','=',session('building_id')],
                    ['b.name','like',$name],
                ];
            }
        }
        if(!isset($map)){
            $map = [
                ['a.building_id','=',session('building_id')]
            ];
        }
        $list =  Db::table('home_member_record')
            ->alias('a')
            ->field('a.*,b.name,e.username')
            ->join('member b','a.vip_id = b.id')
            ->join('admin e','a.operator = e.id')
            ->where($map)
            ->order('create_time', 'asc')
            ->paginate(['list_rows'=> 15,'query' => input('param.')]);
        return view('recharge_record',['list'=>$list]);
    }

    /*----------------------------------------------------------------------------------------------------------------------------------*/
    /*
     * 会员 类型
     * */
    public function viptype()
    {
        $list = Db::name('viptype')
            ->where('building_id',session('building_id'))
            ->order('price desc')
            ->paginate(10);
        return view('viptype',['list' => $list]);
    }

    /*
     * 会员类型 添加
     * */
    public function vipadd()
    {
        if(request()->isAjax()){
            $data = input('param.');
            $data['create_time'] = time();
            $data['building_id'] = session('building_id');
            //判断是否添加成功
            if(Db::name('viptype')->insert($data)){
                return $this->return_json('新增成功','100');
            }else{
                return $this->return_json('新增失败','0');
            }
        }
        return view();
    }

    /*
     * 会员类型编辑
     * */
    public function vipedits()
    {
        if(request()->isAjax()){
            $data = input('param.');

            if(Db::name('viptype')->update($data)){
                return $this->return_json('编辑成功','100');
            }else{
                return $this->return_json('编辑失败','0');
            }
        }
        $list = Db::name('viptype')->where('id',input('id'))->find();
        return view('vipedits',['list'=>$list]);
    }

    /*
     * 会员类型 删除
     * */
    public function vipdelete()
    {
        if(request()->isAjax()){
            $data = input('id');
            if( Db::table('viptype')->delete($data)){
                return $this->return_json('删除成功','100');
            }else{
                return $this->return_json('删除失败','0');
            }
        }
    }

}
