<?php
namespace app\home\controller;


use app\index\controller\Basics;
use app\index\validate\Classes;
use think\facade\Db;

/*
 * 班次设置
 *
 * */
class Classe extends Basics
{

    // 初始化
    protected function initialize()
    {
        //初始化模型
        $this->model_name = 'Classes';
        $this->new_model();
        $this->validate = new Classes();
        parent::initialize();
    }

    /*
     * 班次首页
     * */
    public function index()
    {
        $list = Db::table('classes')->where('building_id',session('building_id'))->paginate(10);

        return view('index',['list' => $list]);
    }

    /*
     * 班次添加
     * */
    public function adds()
    {
       if(request()->isAjax()){
           $data = input('param.');
           if(!$this->validate->check($data)){
               return $this->return_json($this->validate->getError(),'0');
           }
           $data['create_time'] = time();
           $data['building_id'] = session('building_id');

           if(Db::table('classes')->insert($data)){
               return $this->return_json('新增成功','100');
           }else{
               return $this->return_json('新增失败','0');
           }
       }
       return view();
    }

    /*
     * 班次编辑
     * */
    public function edits()
    {
        $list = $this->select_find('classes',['id' => input('id')]);

        if(request()->isAjax()){
            $data  = input('param.');
            return $this->db_edit('classes');
        }
        return view('edits',['list' => $list]);
    }

}
