<?php
namespace app\home\controller;


use app\index\controller\Basics;
use app\index\validate\Goodss;
use think\facade\Db;


/*
 * 物品租借
 *
 * */

class Lease extends Basics
{

    // 初始化
    protected function initialize()
    {
        parent::initialize();
    }

    /*
     * 显示页面
     *
     * */
    public function index()
    {
        $list = Db::table('home_lease_goods')->where('building_id',session('building_id'))->paginate(10);

        return view('index',['list' => $list]);
    }

    /*
     * 添加可租借物品
     * */
    public function adds()
    {
        $data = input('param.');
        if(request()->isAjax()){
            $data['building_id'] = session('building_id');
            $data['create_time'] = time();
            if( Db::table('home_lease_goods')->insert($data)){
                return $this->return_json('新增成功','100');
            }else{
                return $this->return_json('新增失败','0');
            }
        }
        return view();
    }

    /*
     * 租借记录
     * */
    public function rental_records()
    {
        if(request()->isGet()){
            $data = input('param.');
            if(!empty($data['start']) && !empty($data['end'])){
                $data['start'] = strtotime($data['start']);
                $data['end'] = strtotime($data['end']);
                $map = [
                    ['a.building_id','=',session('building_id')],
                    ['a.create_time','>',$data['start']],
                    ['a.create_time','<',$data['end']]
                ];
            }
            if(!empty($data['room_num'])){
                $name = '%'.$data['room_num']."%";
                $map = [
                    ['a.building_id','=',session('building_id')],
                    ['b.room_num','like',$name],
                ];
            }
        }
        if(!isset($map)){
            $map = [
                ['a.building_id','=',session('building_id')]
            ];
        }
        $list =  Db::table('home_lease_records')
            ->alias('a')
            ->field('a.*,b.room_num,c.goods_name')
            ->join('room b','a.room_id = b.id')
            ->join('home_lease_goods c','a.goods_id = c.id')
            ->where($map)
            ->paginate(['list_rows'=> 10,'query' => input('param.')]);
       
        return view('rental_records',['id'=>input('id'),'list'=>$list]);
    }

    /*
     * 添加租借
     * */
    public function add_rental()
    {
        if(request()->isAjax()){
            $data = input('param.');
            $data['building_id'] = session('building_id');
            $data['create_time'] = time();
            if( Db::table('home_lease_records')->insert($data)){
                return $this->return_json('新增成功','100');
            }else{
                return $this->return_json('新增失败','0');
            }
        }
        $list = Db::table('home_lease_goods')->where('building_id',session('building_id'))->select();
        return view('add_rental',['list'=>$list,'id'=>input('id')]);
    }

}
