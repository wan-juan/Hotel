<?php
namespace app\api\controller;

use app\BaseController;
use think\facade\Db;

class AppIndex extends Super
{

    /*
     * 查询所有楼栋
     * */
    public function index(){
        if(request()->isPost()){
            $list = Db::table('building')->alias('a')->field('a.building as label,a.id as value')->select();

            return json([
                'msg' => $list,
                'code' => '200',
                'hotel'=> "亚朵酒店",
                'mylocation'=>"临沂府右路与兰陵路交汇"
            ]);
        }
    }


    /*
     * 查询单个酒店
     * */
    public function select_hotel(){
        if(request()->isPost()){
            $res = Db::table('building')->where('building',input('hotel'))->find();
            $this->check_token(input('token'));
            $list =   Db::table('layout')->select();
            $res['list']['layout'] = [];
            $res['list']['count'] = [];
            $res['list']['price'] = [];
            foreach($list  as $v){
                $map = [
                    ['a.building_id','=',$res['id']],
                    ['b.id','=',$v['id']],
                    ['a.status','=','1']
                ];
                $list =  Db::table('room')
                    ->alias('a')
                    ->field('a.*,b.type_name,b.price,b.deposit,b.hour,c.building,d.storey,e.monday')
                    ->join('layout b','a.type_id = b.id')
                    ->join('building c','a.building_id = c.id')
                    ->join('storey d','a.storey_id = d.id')
                    ->join('week e','a.id = e.layout_id')
                    ->where($map)
                    ->select();
                $count =  Db::table('room')
                    ->alias('a')
                    ->field('a.*,b.type_name,b.price,b.deposit,b.hour,c.building,d.storey,e.monday')
                    ->join('layout b','a.type_id = b.id')
                    ->join('building c','a.building_id = c.id')
                    ->join('storey d','a.storey_id = d.id')
                    ->join('week e','a.id = e.layout_id')
                    ->where($map)
                    ->count();

                if(isset($list[0])){

                    array_push($res['list']['layout'],$list[0]['type_name']);
                    array_push($res['list']['price'],$list[0]['price']);
                    array_push($res['list']['count'],$count);
                }
            }

            return $this->return_json($res,'200');
        }

    }


    /*
     * 房间付款
     * */
    public function room_pay(){
        if(request()->isPost()){
            $data = input('param.');
            $this->check_token($data['token']);
            //查询房型的id
            $layout = Db::table('layout')->where('type_name',$data['layout'])->find();
            //查询楼栋的id
            $building = Db::table('building')->where('building',$data['hotel'])->find();

            //查询房间
            $map = [
                ['building_id','=',$building['id']],
                ['type_id','=',$layout['id']]
            ];

            $room = Db::table('room')->where($map)->find();
            $data['create_time'] = time();
            $data['layout_id'] = $layout['id'];
            $data['building_id'] = $building['id'];
            $data['order_num'] = date('ymdHis').rand(1000,9999);
            if(Db::table('app_subscribe')->insert($data)){
                return $this->return_json('支付成功','200');
            }else{
                return $this->return_json('支付失败','0');
            }
        }

    }

    /*
     * 查询订单
     * */
    public function select_order(){
        if(request()->isPost()){
            $this->check_token(input('token'));
            if(input('types') == 'all'){
                $list = Db::table('app_subscribe')->where('user_id',input('user_id'))->select();
            }elseif(input('types')=='unpaid'){
                $map = [
                    ['pay_status','=','0'],
                    ['user_id','=',input('user_id')]
                ];
                $list = Db::table('app_subscribe')->where($map)->select();
            }

            return $this->return_json($list,'200');
        }
    }

}
